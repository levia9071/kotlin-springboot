# KotlinでSpringBootを使ってみた
## 【環境】
* IntelliJ IDEA 2020.3.1 (Community Edition)
* JDK 1.8
* ポスグレ
## 【目標】
ポスグレ使って、DB操作する
## 【やること】
1. http/localhost:8080/testが起動できることを確認
1. ポスグレを準備
1. ポスグレに接続確認（参照）
1. 追加／更新／削除

---
## 1. http/localhost:8080/testが起動できることを確認
### ■Spring Initializerからダウンロード
https://start.spring.io/
* Project：Gradle Project
* Language：**Kotlin**  
* SpringBoot：2.4.1
* Project Metadata：好きにする
* Packaging：好きにする。私はJar
* Java 8
* Dependencies:SpringWeb

ダウンロード・解凍・IntelliJで開くまではセット。

### ■（自動生成）Applicationクラス
#### Kotlin
```kotlin
package com.levia.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinApplication

fun main(args: Array<String>) {
	runApplication<KotlinApplication>(*args)
}
```
#### Java
```Java
package com.levia.kotlin;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.runApplication;

@SpringBootApplication
class KotlinApplication(){
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
```

### ■ Controllerクラス
#### Kotlin
```kotlin
package com.levia.kotlin.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SampleController {
    @GetMapping("/test")
    fun test() : String{
        return "test"
    }
}
```
#### Java
```java
package com.levia.kotlin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class SampleController() {
    @GetMapping("/test")
    public String test(){
        return "test";
    }
}
```